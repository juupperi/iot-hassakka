'use strict'

module.exports = function(app) {

  var temperatureController = require('../controllers/temperatureTestController');

  app.route('/temperature/test')
    .get(temperatureController.getTemperature);

  app.route('/temperature/test/hourly')
    .get(temperatureController.getTemperatureHourly);
  
  app.route('/temperature/mqtt/')
    .get(temperatureController.getLatestMqttTemperature);
  
  app.route('/temperature/mqtt/hourly')
    .get(temperatureController.getMqttTemperatureHourly);

};