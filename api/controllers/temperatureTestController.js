'use strict'


// -------------------------------------------------------------------
const mqtt = require('mqtt')
const client = mqtt.connect('mqtt://test.mosquitto.org')
client.on('connect', () => {
  client.subscribe('test/temperature')
});

client.on('message', (topic, message) => {
  if(topic == 'test/temperature'){
    var date = new Date().toISOString();
    hourlyMqqtInputlList.push(
      {
        temperature: parseFloat(message.toString()),
        time: date
      }
    );
    latestMqqtInput = {
      temperature: parseFloat(message.toString()),
      time: date
    };
  }
});

//-----------------------------------------------------------------------
// CRON JOBS

var cronJob = require('cron').CronJob;
var jobUpdateRealMqttFeed = new cronJob('* * * * * ', () => {
  console.log(" JOB RUNNING");
  var oldestData1 = hourlyFeed.shift();
  var combinedTemp1 = 0;
  for(var i = 0; i <hourlyMqqtInputlList.length; i++) {
    combinedTemp1 += hourlyMqqtInputlList[i].temperature;
  }
  var hourlyAverageTemp1 =combinedTemp1 / hourlyMqqtInputlList.length;
  hourlyFeed.push({
    hour: oldestData1.hour,
    averageTemperature: hourlyAverageTemp1
  });
  hourlyMqqtInputlList = [];
}, null, true, 'Europe/Helsinki');
jobUpdateRealMqttFeed.start();



var job2 = new cronJob('*/5 * * * * *', () => {
  console.log("JOB 2 RUNNING");
  lastHourMqttTestFeed.push(generateMqttTestInput());
}, null, true, 'Europe/Helsinki');
job2.start();



var job3 = new cronJob('*/30 * * * * *', () => {
  console.log("JOB 3 RUNNING");
  var oldestData = hourlyTestFeed.shift();
  var combinedTemp = 0;
  for(var i = 0; i <lastHourMqttTestFeed.length; i++) {
    combinedTemp += lastHourMqttTestFeed[i].temperature;
  }
  var hourlyAverageTemp =combinedTemp / lastHourMqttTestFeed.length;
  hourlyTestFeed.push({
    hour: oldestData.hour,
    averageTemperature: hourlyAverageTemp
  });
  lastHourMqttTestFeed = [];
}, null, true, 'Europe/Helsinki');




var randomizeHourlyTestFeed = function() {
  var testFeed = [];
  var i = 0
  while(i <= 23){
    testFeed.push(
      {
        hour: i,
        averageTemperature: Math.floor(Math.random() * 100)
      }
    );
    i++;
  }
  return testFeed;
}

var generateMqttTestInput = function() {
  var testDate = new Date().toISOString();
  var testValue = {
    temperature: Math.floor(Math.random() * 100),
    time: testDate
  }
  return testValue;

  
}

//-----------------------------------------------------------------------

// serves latest item from mqtt feed
exports.getTemperature = function(req, res) {
  res.json(lastHourMqttTestFeed[lastHourMqttTestFeed.length - 1])
};


// serves list from mqtt feed
exports.getTemperatureHourly = function(req, res) {
  res.json(hourlyTestFeed)
};

exports.getMqttTemperatureHourly = function(req, res) {
  res.json(hourlyFeed)
}

exports.getLatestMqttTemperature = function(req, res) {
  res.json(latestMqqtInput)
}



var date1 = new Date().toISOString();
var hourlyMqqtInputlList = [
  {
  temperature: 12,
  time: date1
  }
];

var latestMqqtInput = {
  temperature: 12,
  time: date1
  };

var lastHourMqttTestFeed = [];

var hourlyTestFeed = [
  {
  hour: 0,
  averageTemperature: 96
  },
  {
  hour: 1,
  averageTemperature: 19
  },
  {
  hour: 2,
  averageTemperature: 50
  },
  {
  hour: 3,
  averageTemperature: 11
  },
  {
  hour: 4,
  averageTemperature: 94
  },
  {
  hour: 5,
  averageTemperature: 80
  },
  {
  hour: 6,
  averageTemperature: 6
  },
  {
  hour: 7,
  averageTemperature: 77
  },
  {
  hour: 8,
  averageTemperature: 61
  },
  {
  hour: 9,
  averageTemperature: 72
  },
  {
  hour: 10,
  averageTemperature: 75
  },
  {
  hour: 11,
  averageTemperature: 41
  },
  {
  hour: 12,
  averageTemperature: 49
  },
  {
  hour: 13,
  averageTemperature: 57
  },
  {
  hour: 14,
  averageTemperature: 37
  },
  {
  hour: 15,
  averageTemperature: 78
  },
  {
  hour: 16,
  averageTemperature: 17
  },
  {
  hour: 17,
  averageTemperature: 76
  },
  {
  hour: 18,
  averageTemperature: 94
  },
  {
  hour: 19,
  averageTemperature: 71
  },
  {
  hour: 20,
  averageTemperature: 4
  },
  {
  hour: 21,
  averageTemperature: 63
  },
  {
  hour: 22,
  averageTemperature: 49
  },
  {
  hour: 23,
  averageTemperature: 97
  }
  ];

  var hourlyFeed = [
    {
    hour: 0,
    averageTemperature: 96
    },
    {
    hour: 1,
    averageTemperature: 19
    },
    {
    hour: 2,
    averageTemperature: 50
    },
    {
    hour: 3,
    averageTemperature: 11
    },
    {
    hour: 4,
    averageTemperature: 94
    },
    {
    hour: 5,
    averageTemperature: 80
    },
    {
    hour: 6,
    averageTemperature: 6
    },
    {
    hour: 7,
    averageTemperature: 77
    },
    {
    hour: 8,
    averageTemperature: 61
    },
    {
    hour: 9,
    averageTemperature: 72
    },
    {
    hour: 10,
    averageTemperature: 75
    },
    {
    hour: 11,
    averageTemperature: 41
    },
    {
    hour: 12,
    averageTemperature: 49
    },
    {
    hour: 13,
    averageTemperature: 57
    },
    {
    hour: 14,
    averageTemperature: 37
    },
    {
    hour: 15,
    averageTemperature: 78
    },
    {
    hour: 16,
    averageTemperature: 17
    },
    {
    hour: 17,
    averageTemperature: 76
    },
    {
    hour: 18,
    averageTemperature: 94
    },
    {
    hour: 19,
    averageTemperature: 71
    },
    {
    hour: 20,
    averageTemperature: 4
    },
    {
    hour: 21,
    averageTemperature: 63
    },
    {
    hour: 22,
    averageTemperature: 49
    },
    {
    hour: 23,
    averageTemperature: 97
    }
    ];


